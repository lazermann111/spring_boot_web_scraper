package com.lazermann.AddApplication.dao;

import com.lazermann.AddApplication.DozerHelper;
import com.lazermann.AddApplication.dto.UrlResBaseDto;
import com.lazermann.AddApplication.dto.UrlResDto;
import com.lazermann.AddApplication.dto.UserBaseDto;
import com.lazermann.AddApplication.dto.UserDto;
import com.lazermann.AddApplication.model.Snapshot;
import com.lazermann.AddApplication.model.UrlRes;
import com.lazermann.AddApplication.model.User;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Repository
@Transactional
public class UserDao {

    @Autowired
    private SessionFactory _sessionFactory;
    @Autowired
    DozerBeanMapper dozerMapper;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public long save(User user) {
     getSession().save(user);
     return user.getId();
        //return;
    }

    public void fillDb() throws Exception {
        Snapshot snapshot = new Snapshot(System.currentTimeMillis(), "SomeCont");
        UrlRes resource = new UrlRes();
        resource.setUrl("https://google.com");
        resource.setSnapshots(Collections.singletonList(snapshot));

        User user = new User("John", "doe@gmail.com");
        user.setUpdateInterval(8000);
        user.setResources(Collections.singletonList(resource));

        getSession().save(user);



    }

    public void delete(User user) {
        getSession().delete(user);

    }



    @SuppressWarnings("unchecked")
    public List<UserDto> getAll() {

        List<User> res = getSession().createQuery("from User").list();
        return DozerHelper.map(dozerMapper, res, UserDto.class);
    }

    @SuppressWarnings("unchecked")
    public UserDto getByUsername(String uname) {


        User res =  (User) getSession().createQuery(
                "from User where username = :uname")
                .setParameter("uname", uname)
                .uniqueResult();

        return dozerMapper.map(res, UserDto.class);
    }

    public UserDto getById(long id) {

        User res =  (User) getSession().load(User.class, id);
        return dozerMapper.map(res, UserDto.class);
    }

    /*public User getUserById(long id) {

        User res =  (User) getSession().load(User.class, id);
        return res;
    }*/
    @SuppressWarnings("unchecked")
    public User getUserById(long id) {
        User user = (User) getSession().createQuery(
                "from User where id = :id")
                .setParameter("id", id)
                .uniqueResult();
        return user;
        //return dozerMapper.map(user, UserDto.class);
    }

    public void update(User user) {
        getSession().update(user);
    }

    public void update(List<UserDto> users) {
        for (UserDto u : users)
        {
          getSession().update(dozerMapper.map(u, User.class));
        }
    }

    @SuppressWarnings("unchecked")
    public UserDto getUser(String login) {
        User user = (User) getSession().createQuery(
                "from User where username = :login")
                .setParameter("login", login)
                .uniqueResult();
        return dozerMapper.map(user, UserDto.class);
    }

    @SuppressWarnings("unchecked")
    public List<UrlResDto> getSnapshots(long id) throws Exception{
        /*return getSession().createQuery(
                "select user, " +
                        "urlres " +
                       // "snapshot " +
                        "from User user, UrlRes urlres " +
                        "where urlres.user_id=user.id")
                //"from User where id = :id")
                //.setParameter("id", id)
                .list();*/
        List<UrlRes> list = getSession().createQuery(
                "select user.resources from User user where user.id=:id")
                .setParameter("id", id)
                .list();
        return DozerHelper.map(dozerMapper, list, UrlResDto.class);
    }

    public UrlResDto getUrlRes(long userId, long urlId) {
        UrlRes urlRes = (UrlRes) getSession().createQuery(
                "select url from User user join user.resources url where user.id=:userId and url.id=:urlId")
                //"select user.resources from User user join user.resources url where user.id=:userId and url.id=:urlId")
                .setParameter("userId", userId)
                .setParameter("urlId", urlId)
                //.setMaxResults(1)
                .uniqueResult();
        return dozerMapper.map(urlRes, UrlResDto.class);

    }

    public List<UrlResBaseDto> getUrls(long userId) {
        List<UrlRes> list = getSession().createQuery(
                "select user.resources from User user where user.id=:id")
                .setParameter("id", userId)
                .list();
        return DozerHelper.map(dozerMapper, list, UrlResBaseDto.class);
    }

    public void addUrl(long userId, String url) {
        UrlRes resource = new UrlRes();
        resource.setUrl(url);
        resource.setInvalid(false);

        User user = getUserById(userId);
        user.getResources().add(resource);

        getSession().update(user);
    }

    public void updateInterval(long userId, long interval) {
        User user = (User) getSession().createQuery(
                "from User where id = :id")
                .setParameter("id", userId)
                .uniqueResult();
        user.setUpdateInterval(interval);
        getSession().update(user);
    }

    public List<UserBaseDto> getAllUsers() {
        List<User> res = getSession().createQuery("from User").list();
        return DozerHelper.map(dozerMapper, res, UserBaseDto.class);

    }

    public void createUser(String userName, String password, long updateInterval) {
        User user = new User(userName, password);
        user.setUpdateInterval(updateInterval);

        getSession().save(user);
    }
}
