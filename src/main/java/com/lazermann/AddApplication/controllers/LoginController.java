package com.lazermann.AddApplication.controllers;

import com.lazermann.AddApplication.dao.UserDao;
import com.lazermann.AddApplication.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoginController {
    @Autowired
    UserDao userDao;

    @Autowired
    UserService userService;

    @RequestMapping(value="/login", method = RequestMethod.POST)
    public ResponseEntity login(String username, String password) {
        ResponseEntity requestEntity = null;
        try {
            requestEntity = userService.getUser(username, password);
        }
        catch (Exception ex) {
            requestEntity = new ResponseEntity<>(-1, HttpStatus.BAD_REQUEST);
        }
        return requestEntity;
    }

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public ResponseEntity register(String username, String password) {
        ResponseEntity requestEntity = null;
        try {
            requestEntity = userService.saveUser(username, password);
        }
        catch (Exception ex) {
            requestEntity = new ResponseEntity<>("Error: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return requestEntity;
    }

}
