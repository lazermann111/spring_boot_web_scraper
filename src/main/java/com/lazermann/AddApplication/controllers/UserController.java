package com.lazermann.AddApplication.controllers;

import com.lazermann.AddApplication.dao.UserDao;
import com.lazermann.AddApplication.dto.UserDto;
import com.lazermann.AddApplication.model.User;
import com.lazermann.AddApplication.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/user")
public class UserController {

    @Autowired
    private UserDao _userDao;

    @Autowired
    private UserService userService;

    @PostMapping("/sign-up")
    public void signUp(@RequestBody User user) {
        //user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        //applicationUserRepository.save(user);
    }

    @RequestMapping(value="/delete", method = RequestMethod.POST)
    public String delete(long id) {
        try {
            User user = new User(id);
            //_userDao.delete(user);
            userService.delete(user);

        }
        catch(Exception ex) {
            return ex.getMessage();
        }
        return "User succesfully deleted!";
    }

    @RequestMapping(value="/get-by-name", method = RequestMethod.GET)
    public ResponseEntity<UserDto> getByName(String username) {
        UserDto user;
        try {
            user  = _userDao.getByUsername(username);

        }
        catch(Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
          return new ResponseEntity<>(user,HttpStatus.OK);
    }

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public String create(String email, String name) {
        try {
            User user = new User(email, name);
            _userDao.save(user);
        }
        catch(Exception ex) {
            return ex.getMessage();
        }
        return "User succesfully saved!";
    }
    @RequestMapping(value="/cheat", method = RequestMethod.GET)
    public String cheat() {
        try {

            //_userDao.fillDb();
            userService.fillDB();
        }
        catch(Exception ex) {
            return ex.getMessage();
        }
        return "User succesfully saved!";
    }

    @RequestMapping(value ="/createUser", method = RequestMethod.POST)
    public ResponseEntity createUser(String userName, String password, String updateInterval) {
        ResponseEntity responseEntity = null;
        try {
            userService.createUser(userName, password, Long.parseLong(updateInterval));
            responseEntity = new ResponseEntity<>("User created successfully!", HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>("Problem: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @RequestMapping(value ="/getAllUsers", method = RequestMethod.GET)
    public ResponseEntity getAllUsers() {
        ResponseEntity responseEntity = null;
        try {
            responseEntity = new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>("Problem: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @RequestMapping(value="/getSnapshots", method = RequestMethod.GET)
    public ResponseEntity getSnapshots(String userId, String urlId) {
        ResponseEntity responseEntity = null;
        try{
            responseEntity = new ResponseEntity<>(userService.getSnapshots(Long.parseLong(userId), Long.parseLong(urlId)), HttpStatus.OK);
        }
        catch (IllegalArgumentException ex) {
            return new ResponseEntity<>("This user don't exist!" , HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            return new ResponseEntity<>("Problem: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @RequestMapping(value="/getLastSnapshot", method = RequestMethod.GET)
    public ResponseEntity getLastSnapshot(String userId, String urlId) {
        ResponseEntity responseEntity = null;
        try{
            responseEntity = new ResponseEntity<>(userService.getLastSnapshot(Long.parseLong(userId), Long.parseLong(urlId)), HttpStatus.OK);
        }
        catch (IllegalArgumentException ex) {
            return new ResponseEntity<>("This user don't exist!" , HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            return new ResponseEntity<>("Problem: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @RequestMapping(value="/getUrls", method = RequestMethod.GET)
    public ResponseEntity getUrls(String userId) {
        ResponseEntity responseEntity = null;
        try{
            responseEntity = new ResponseEntity<>(userService.getUrls(Long.parseLong(userId)), HttpStatus.OK);
        }
        catch (IllegalArgumentException ex) {
            return new ResponseEntity<>("This user don't exist!" , HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            return new ResponseEntity<>("Problem: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @RequestMapping(value = "/addUrl", method = RequestMethod.POST)
    public ResponseEntity addUrl(String userId, String url) {
        ResponseEntity responseEntity = null;
        try{
            userService.addUrl(Long.parseLong(userId), url);
            responseEntity = new ResponseEntity<>("Added", HttpStatus.OK);
        }
        catch (IllegalArgumentException ex) {
            return new ResponseEntity<>("This user don't exist!" , HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            return new ResponseEntity<>("Problem: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @RequestMapping(value = "/deleteUrl", method = RequestMethod.POST)
    public ResponseEntity deleteUrl(String userId, String urlId) {
        ResponseEntity responseEntity = null;
        try{
            userService.deleteUrl(Long.parseLong(userId), Long.parseLong(urlId));
            responseEntity = new ResponseEntity<>("Removed", HttpStatus.OK);
        }
        catch (IllegalArgumentException ex) {
            return new ResponseEntity<>("This user don't exist!" , HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            return new ResponseEntity<>("Problem: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @RequestMapping(value = "/setUpdateInterval", method = RequestMethod.POST)
    public ResponseEntity setUpdateInterval(String userId,String interval) {
        ResponseEntity responseEntity = null;
        try{
            userService.updateInterval(Long.parseLong(userId),Long.parseLong(interval));
            responseEntity = new ResponseEntity<>("Interval updated!", HttpStatus.OK);
        }
        catch (IllegalArgumentException ex) {
            return new ResponseEntity<>("This user don't exist!" , HttpStatus.BAD_REQUEST);
        }
        catch (Exception ex) {
            return new ResponseEntity<>("Problem: " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }



}