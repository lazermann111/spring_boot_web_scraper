package com.lazermann.AddApplication.model;


import javax.persistence.*;


@Entity
@Table(name="snapshot")
public class Snapshot
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long timestamp;

    @Column(length=1000000)
    private String content;

    public Snapshot() {

    }

    public Snapshot(long timestamp, String content) {
        this.timestamp = timestamp;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
