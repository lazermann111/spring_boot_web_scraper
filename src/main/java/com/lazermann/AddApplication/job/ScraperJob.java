package com.lazermann.AddApplication.job;


import com.lazermann.AddApplication.dao.UserDao;
import com.lazermann.AddApplication.dto.SnapShotDto;
import com.lazermann.AddApplication.dto.UrlResDto;
import com.lazermann.AddApplication.dto.UserDto;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.List;

public class ScraperJob extends QuartzJobBean
{

    @Autowired
    private UserDao service;



    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("******************* execute ScraperJob ********************");

        List<UserDto> users = service.getAll();

        for (UserDto u : users)
        {
            if (u.getLastTimeUpdated() + u.getUpdateInterval() > System.currentTimeMillis()) continue;

            for (UrlResDto resource : u.getResources())
            {
                try{
                    Document document = Jsoup.connect(resource.getUrl()).get();

                    SnapShotDto s = new SnapShotDto(System.currentTimeMillis(), document.body().text());
                    resource.getSnapshots().add(s);

                }
                catch(Exception e){         //if an HTTP/connection error occurs, handle JauntException.
                    System.err.println(e);
                    resource.setInvalid(true);
                }


            }


            u.setLastTimeUpdated(System.currentTimeMillis());
        }


        service.update(users);

    }
}