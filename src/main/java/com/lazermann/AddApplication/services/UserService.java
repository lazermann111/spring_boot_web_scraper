package com.lazermann.AddApplication.services;

import com.lazermann.AddApplication.dao.UserDao;
import com.lazermann.AddApplication.dto.*;
import com.lazermann.AddApplication.model.UrlRes;
import com.lazermann.AddApplication.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService
{
    @Autowired
    UserDao userDao;


    public void fillDB() throws Exception {
        userDao.fillDb();
    }

    public void delete(User user) throws Exception {
        userDao.delete(user);
    }

    public void save(User user) throws Exception {
        userDao.save(user);
    }

    public ResponseEntity getUser(String login, String password) {
        UserDto user = userDao.getUser(login);
        if(user.getPassword().equals(password))
            return new ResponseEntity<>(user.getId(), HttpStatus.OK);
        else
            return new ResponseEntity<>("Wrong password!", HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity saveUser(String login, String password) {
        User user = new User(login, password);
        long id = userDao.save(user);
        return new ResponseEntity<>(id, HttpStatus.OK);

    }

    public List<SnapShotDto> getSnapshots(long userId, long urlId) throws Exception {
        isUserExist(userId);
        List<UrlResDto> list = userDao.getSnapshots(userId);
        for (UrlResDto urlResDto: list) {
            if(urlResDto.getId() == urlId)
                return urlResDto.getSnapshots();
        }
        return null;
    }

    public SnapShotDto getLastSnapshot(long userId, long urlId) {
        isUserExist(userId);
        UrlResDto urlRes = userDao.getUrlRes(userId, urlId);
        //return urlRes.getSnapshots().get(urlRes.getSnapshots().size()-1); //Last element in the list
        return urlRes.getSnapshots().get(0); //Because new snapshots add to first position
    }

    public List<UrlResBaseDto> getUrls(long userId) throws Exception {
        isUserExist(userId);
        return userDao.getUrls(userId);

    }

    private void isUserExist(long userId) {
        User userDto = userDao.getUserById(userId);
        if(userDto == null)
            throw new IllegalArgumentException();
    }

    public void addUrl(long userId, String url) {
        isUserExist(userId);
        userDao.addUrl(userId, url);
    }

    public void deleteUrl(long userId, long urlId) throws Exception {
        isUserExist(userId);
        User user = userDao.getUserById(userId);
        boolean isHas = false;
        int i = 0;
        for(UrlRes urlRes: user.getResources()) {
            if(urlRes.getId() == urlId) {
                isHas = true;
                break;
            }
            i++;
        }
        if(isHas)
            user.getResources().remove(i);
        userDao.update(user);
    }

    public void updateInterval(long userId, long interval) {
        isUserExist(userId);
        userDao.updateInterval(userId, interval);
    }

    public List<UserBaseDto> getAllUsers() {
        return userDao.getAllUsers();
    }

    public void createUser(String userName, String password, long updateInterval) {
        userDao.createUser(userName, password, updateInterval);
    }
}
