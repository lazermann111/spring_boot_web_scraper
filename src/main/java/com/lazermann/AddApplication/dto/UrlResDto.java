package com.lazermann.AddApplication.dto;

import java.util.List;

public class UrlResDto
{
    private long id;

    private String url;
    private boolean invalid;

    public UrlResDto() {
    }

    private List<SnapShotDto> snapshots;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<SnapShotDto> getSnapshots() {
        return snapshots;
    }

    public void setSnapshots(List<SnapShotDto> snapshots) {
        this.snapshots = snapshots;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }}
