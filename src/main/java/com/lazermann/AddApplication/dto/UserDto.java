package com.lazermann.AddApplication.dto;


import java.util.List;

public class UserDto
{

    private long id;
    private  String username;
    private  String password;
    private  long updateInterval;
    private  long lastTimeUpdated;

   public UserDto(){};

    private List<UrlResDto> resources;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public long getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(long updateInterval) {
        this.updateInterval = updateInterval;
    }

    public long getLastTimeUpdated() {
        return lastTimeUpdated;
    }

    public void setLastTimeUpdated(long lastTimeUpdated) {
        this.lastTimeUpdated = lastTimeUpdated;
    }

    public List<UrlResDto> getResources() {
        return resources;
    }

    public void setResources(List<UrlResDto> resources) {
        this.resources = resources;
    }
}
