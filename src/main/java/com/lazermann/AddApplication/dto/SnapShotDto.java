package com.lazermann.AddApplication.dto;


public class SnapShotDto {

    private long id;

    private long timestamp;
    private String content;
    public SnapShotDto(){};

    public SnapShotDto(long timestamp, String content) {
        this.timestamp = timestamp;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
